#include "levitation.h"
#include <cmath>
#include <iostream>
#include <vector>
#include <complex>
#include <eigen3/Eigen/Dense>
#include <fstream>
#include <iostream>

using namespace std::complex_literals;

void generate_the_matrix(double width, double height, double discretization, double R)
{
    /* Variable Physical Constraints */
    double mm = 1e-3;
    double x_min = -width * mm;
    double x_max = width * mm;
    double z_min = 0.0;
    double z_max = height * mm;
    double disc = discretization * mm;
    double pi = 3.141592654;

    /* Complex Constants */
    std::complex<double> area_transducer(pi * R * R, 0.0);
    std::complex<double> area_refelector(pi * (x_max / 2) * (x_max / 2), 0.0);
    std::complex<double> rho(1.214, 0.0); // Density of air, km/m^3
    std::complex<double> c(340.1, 0.0);   // Speed of sound in air, m/s
    std::complex<double> frequency(28000.0, 0.0);
    std::complex<double> wavelength(c.real() / frequency.real(), 0.0);
    std::complex<double> omega((2.0 * pi * frequency.real()), 0.0); // Angualr frequency
    std::complex<double> num_cells(100.0, 0.0);                     // Number of cells used for calculation
    std::complex<double> unit_cell_transducer = area_transducer / num_cells;
    /* 
    Unit cell area for the reflector is mulitplied by a factor of 4 to keep the area per cell the
    same for both the transducer and reflector
    */
    std::complex<double> unit_cell_reflector = area_refelector / (num_cells * 4.0);
    std::complex<double> primary_wave_constant = (omega * rho * c) / wavelength;
    std::complex<double> wavenumber = omega / c;
    std::complex<double> amplitude(6e-6, 0.0);
    std::complex<double> reflected_wave_constant(0.0, 1.0);

    /* Numeric grid defining area for calculations */
    std::vector<double> x = linspace(x_min, x_max, (x_max - x_min) / disc);
    int size_of_x = x.size();
    std::vector<double> z = linspace(z_min, z_max, (z_max - z_min) / disc);
    int size_of_z = z.size();
    std::vector<double> transducer = linspace(-R, R, (2 * R) / disc);

    /* Indexing values to make things easier to track */
    int N = (2 * R) / disc;
    int M = ((x_max - x_min) / disc) * ((z_max - z_min) / disc);
    int L = (x_max - x_min) / disc;
    int Z = (z_max - z_min) / disc;

    Eigen::MatrixXd r_nm(N, M);
    Eigen::MatrixXd r_im(L, M);
    Eigen::MatrixXd r_in(N, L);
    Eigen::MatrixXd r_ni(L, N);

    Eigen::MatrixXcd T_TM(N, M);
    Eigen::MatrixXcd T_RM(L, M);
    Eigen::MatrixXcd T_TR(N, L);
    Eigen::MatrixXcd T_RT(L, N);
    Eigen::MatrixXcd transducer_amplitude(N, 1);
}

int main(int argc, char *argv[])
{
    if (argc < 6)
    {
        std::cout << "Warning: Not enough input arguments specified, using defaults.\n";
        std::cout << "Usage: ./transucer_calc WIDTH HEIGHT DISCRETIZATION RADIUS FILENAME.txt\n";
        std::cout << "Width: 10mm\n";
        std::cout << "Height: 25mm\n";
        std::cout << "Discretization: 0.5mm\n";
        std::cout << "Tranducer Radius: 5mm\n";
        generate_the_matrix(30, 90, 0.5, 5);
        outfile = "propagation_results.txt";
    }
    else
    {
        generate_the_matrix(atof(argv[1]), atof(argv[2]), atof(argv[3]), atof(argv[4]));
        std::cout << "Width: " << argv[1] << "mm\n";
        std::cout << "Height: " << argv[2] << "mm\n";
        std::cout << "Discretization: " << argv[3] << "mm\n";
        std::cout << "Transducer Radius: " << argv[4] << "mm\n";
        outfile = argv[5];
    }
    std::cout << "Output file: " << outfile << "\n";
    std::cout << "N " << N << "\n";
    std::cout << "M " << M << "\n";
    std::cout << "L " << L << "\n";
    std::cout << "Z " << Z << "\n";
    // // Calculate r_nm
    for (int i = 0; i < N; i++)
    {
        int q = 0;
        for (int j = 0; j < L; j++)
        {
            for (int k = 0; k < Z; k++)
            {
                r_nm(i, q) = sqrt((transducer[i] - x[j]) * (transducer[i] - x[j]) + (z_max - z[k]) * (z_max - z[k]));
                q++;
            }
        }
    }
    // r_im
    for (int i = 0; i < L; i++)
    {
        int q = 0;
        for (int j = 0; j < L; j++)
        {
            for (int k = 0; k < Z; k++)
            {
                r_im(i, q) = sqrt((x[i] - x[j]) * (x[i] - x[j]) + (z_min - z[k]) * (z_min - z[k]));
                q++;
            }
        }
    }

    // r_in
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < L; j++)
        {
            r_in(i, j) = sqrt((transducer[i] - x[j]) * (transducer[i] - x[j]) + (z_max - z_min) * (z_max - z_min));
        }
    }

    // r_ni
    r_ni = r_in.transpose();

    std::cout << "Finished builded distance matricies\n";
    reflected_wave_constant /= wavelength;

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++)
        {
            T_TM(i, j) = (unit_cell_transducer * exp(std::complex<double>(0.0, -1.0) * wavenumber * r_nm(i, j))) / r_nm(i, j);
        }

        for (int k = 0; k < L; k++)
        {
            T_TR(i, k) = (unit_cell_transducer * exp(std::complex<double>(0.0, -1.0) * wavenumber * r_in(i, k))) / r_in(i, k);
        }
    }

    for (int i = 0; i < L; i++)
    {
        for (int j = 0; j < N; j++)
        {
            T_RT(i, j) = (unit_cell_reflector * exp(std::complex<double>(0.0, -1.0) * wavenumber * r_ni(i, j))) / r_ni(i, j);
        }
    }
    int h = 0;
    for (int i = 0; i < L; i++)
    {
        for (int j = 0; j < M; j++)
        {
            if (r_im(i, j) == 0.0)
            {
                continue;
            }
            else
            {
                T_RM(i, j) = (unit_cell_reflector * exp(std::complex<double>(0.0, -1.0) * wavenumber * r_im(i, j))) / r_im(i, j);
            }
        }
    }
    
    std::cout << "Finished building transfer matricies\n";
    auto T_TMT = T_TM.transpose();
    auto T_TRT = T_TR.transpose();
    auto T_RTT = T_RT.transpose();
    auto T_RMT = T_RM.transpose();
    std::cout << "Calculating propagation...\n";

    for (int i = 0; i < N; i++)
    {
        transducer_amplitude(i, 0) = amplitude * exp(std::complex<double>(0, omega.real()));
    }
    Eigen::MatrixXcd propagation = (primary_wave_constant * reflected_wave_constant) * T_RMT * (T_TRT * transducer_amplitude) +
                                   (primary_wave_constant * reflected_wave_constant * reflected_wave_constant) * (T_TMT * T_RTT) * (T_TRT * transducer_amplitude) +
                                   (primary_wave_constant * reflected_wave_constant * reflected_wave_constant * reflected_wave_constant) * T_RMT * (T_TRT * T_RTT) * (T_TRT * transducer_amplitude) +
                                   (primary_wave_constant * reflected_wave_constant * reflected_wave_constant * reflected_wave_constant * reflected_wave_constant) * (T_TMT * T_RTT) * (T_TRT * T_RTT) * (T_TRT * transducer_amplitude);
    FILE *pFile;
    pFile = fopen(outfile.c_str(), "w");
    std::cout << "Writing results to file. ";
    fprintf(pFile, "%d %d %f %f %f %f %f\n", size_of_x, size_of_z, x_min, x_max, z_min, z_max, disc);
    for (int i = 0; i < propagation.rows(); i++)
    {
        for (int j = 0; j < propagation.cols(); j++)
        {
            fprintf(pFile, "%1.15f\n", propagation(i, j).real());
        }
    }
    std::cout << "Saved results to " << outfile << "\n";
}