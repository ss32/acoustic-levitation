# Acoustic Levitation

Python 3 and C++ Implementation of [Matrix Method for Acoustic Levitation Simulation](https://www.researchgate.net/publication/224254694_Matrix_Method_for_Acoustic_Levitation_Simulation) with a Python 3 script for visualization of C++ results.

---

## Overview

The Python version was originally written as a computational model for a senior physics lab.  The model was verified against an experimental setup and found to accurately predict the peaks and troughs of the standing pressure wave.  I've included it here for posterity

The C++ code is more recent and served as a means to learn the Eigen library.  Until I can find a nice C++ native visualization library, the visualization script is used to parse the output and produce plots.

## Install / Build

[Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) is required to build the C++ version and can be installed with

```bash
sudo apt install libeigen3-dev
```

Build the binary with

```bash
g++ levitation.cpp -I ./include/ -o tranducer_calc
```

## Run and Visualize

By default the resulting binary can be run on its own with no command-line arguments and will output the results to `propagation_results.txt`.  Optional positional arguments are shown when none are supplied.

```
$ ./transducer_calc 
Warning: Not enough input arguments specified, using defaults.
Usage: ./transucer_calc WIDTH HEIGHT DISCRETIZATION RADIUS FILENAME.txt
Width: 10mm
Height: 25mm
Discretization: 0.5mm
Tranducer Radius: 5mm
Output file: propagation_results.txt
N 20
M 21600
L 120
Z 180
Finished builded distance matricies
Finished building transfer matricies
Calculating propagation...
Writing results to file.Saved results to propagation_results.txt
```

Use the `visualize_results.py` script to produce a plot of the results which will be saved in this directory.

```bash
$ python3 visualize_results.py propagation_results.txt 
```

![Default Plot](visualization.png)



