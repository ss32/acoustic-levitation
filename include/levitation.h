#include <vector>
#include <complex>
#include <eigen3/Eigen/Dense>

template <typename T>
std::vector<double> linspace(T start_in, T end_in, int num_in)
{

    std::vector<double> linspaced;

    double start = static_cast<double>(start_in);
    double end = static_cast<double>(end_in);
    double num = static_cast<double>(num_in);

    if (num == 0)
    {
        return linspaced;
    }
    if (num == 1)
    {
        linspaced.push_back(start);
        return linspaced;
    }

    double delta = (end - start) / (num - 1);

    for (int i = 0; i < num - 1; ++i)
    {
        linspaced.push_back(start + delta * i);
    }
    linspaced.push_back(end); // I want to ensure that start and end
                              // are exactly the same as the input
    return linspaced;
}

std::string outfile = "";

/* Variable Physical Constraints */
double mm = 1e-3;
double width = 30.0;
double height = 90.0;
double discretization = 0.5;

double x_min = -width * mm;
double x_max = width * mm;
double z_min = 0.0;
double z_max = height * mm;
double disc = discretization * mm;
double R = 5.0 * mm;
double pi = 3.141592654;

/* Complex Constants */
std::complex<double> area_transducer(pi * R * R, 0.0);
std::complex<double> area_refelector(pi * (x_max / 2) * (x_max / 2), 0.0);
std::complex<double> rho(1.214, 0.0); // Density of air, km/m^3
std::complex<double> c(340.1, 0.0);   // Speed of sound in air, m/s
std::complex<double> frequency(28000.0, 0.0);
std::complex<double> wavelength(c.real() / frequency.real(), 0.0);
std::complex<double> omega((2.0 * pi * frequency.real()), 0.0); // Angualr frequency
std::complex<double> num_cells(100.0, 0.0);                     // Number of cells used for calculation
std::complex<double> unit_cell_transducer = area_transducer / num_cells;
/* 
Unit cell area for the reflector is mulitplied by a factor of 4 to keep the area per cell the
same for both the transducer and reflector
*/
std::complex<double> unit_cell_reflector = area_refelector / (num_cells * 4.0);
std::complex<double> primary_wave_constant = (omega * rho * c) / wavelength;
std::complex<double> wavenumber = omega / c;
std::complex<double> amplitude(6e-6, 0.0);
std::complex<double> reflected_wave_constant(0.0, 1.0);

/* Numeric grid defining area for calculations */
std::vector<double> x = linspace(x_min, x_max, (x_max - x_min) / disc);
int size_of_x = x.size();
std::vector<double> z = linspace(z_min, z_max, (z_max - z_min) / disc);
int size_of_z = z.size();
std::vector<double> transducer = linspace(-R, R, (2 * R) / disc);

/* Indexing values to make things easier to track */
int N = (2 * R) / disc;
int M = ((x_max - x_min) / disc) * ((z_max - z_min) / disc);
int L = (x_max - x_min) / disc;
int Z = (z_max - z_min) / disc;

Eigen::MatrixXd r_nm(N, M);
Eigen::MatrixXd r_im(L, M);
Eigen::MatrixXd r_in(N, L);
Eigen::MatrixXd r_ni(L, N);

Eigen::MatrixXcd T_TM(N, M);
Eigen::MatrixXcd T_RM(L, M);
Eigen::MatrixXcd T_TR(N, L);
Eigen::MatrixXcd T_RT(L, N);
Eigen::MatrixXcd transducer_amplitude(N, 1);
