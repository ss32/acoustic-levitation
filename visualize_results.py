#!/usr/bin/python3 

import matplotlib.pylab as plt
import argparse
import re
import numpy as np
import os

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('resultsFile', type=str,
                        help="Input file, should be a .txt")
    parser.add_argument('--output-folder', type=str, default='./',
                        help="Output directory for saved image.  Default is current working directory.")

    args = parser.parse_args()

    if not os.path.exists(args.output_folder):
        os.mkdir(args.output_folder)

    with open(args.resultsFile, 'r') as f:
        lines = f.readlines()
        params = lines[0].split(' ')
        size_of_x = int(params[0])
        size_of_z = int(params[1])
        x_min = float(params[2])
        x_max = float(params[3])
        z_min = float(params[4])
        z_max = float(params[5])
        discretization = float(params[6])
        x = np.linspace(x_min, x_max, int((x_max-x_min)/discretization))
        z = np.linspace(z_min, z_max, int((z_max-z_min)/discretization))
        pressure = []
        for i,line in enumerate(lines):
            if(i == 0):
                continue
            pressure.append(float(line))
        pressure = np.array(pressure)
        P2 = np.array(np.reshape(pressure, (size_of_x, size_of_z)))
        P2 = np.transpose(P2)
        plt.pcolormesh(x, z, P2)
        plt.title('Pressure Distribution')
        plt.xlabel('X (m)')
        plt.ylabel('Z (m)')
        # Scale the pressure gradients so the colors make sense
        plt.clim(P2.min()/2, -P2.min()/2.5)
        plt.colorbar().set_label('Relative Pressure')
        img_save_path = os.path.join(args.output_folder,'visualization.png')
        plt.savefig(img_save_path,dpi=300)	# Save the image with given parameters
        plt.show()

if __name__ == "__main__":
    main()
